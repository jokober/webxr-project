/*
  Main controller
*/

const UPDATE_TIME = 500; //advertisement detection interval in ms
const CONFIDENCE_THRESHOLD = 0.8; //minimal cofidence

let streamHandler = new StreamHandler(); //handles camera input and taking snapshots
let billboardDetection = new BillboardDetection(); //handles detection requests of billboards
let viewController = new ViewController('scene'); //handles displaying of AR advertisement content

document.addEventListener("DOMContentLoaded", onload);

/*
  Load the video source/camera and starting update method
*/
function onload(){
  let source = getValue()['source'];

  //if source was selected
  if(source != null){
    hideSourceSelector();
    streamHandler.setVideoSource(source, function(){
      refreshContainerSize();
      setInterval(update, UPDATE_TIME);
    });
  }
}

/*
  Update is requested every UPDATE_TIME ms
  Taking frame from streamhandler and pass it to the billboard detection
  response from billboard detection will be used to load the appropriate advertisement
*/

function update(){
  streamHandler.getBlob((frame)=>{
    billboardDetection.getPredictions(frame, (predictions)=>{
      if(predictions != null){
        for(let i=0; i<predictions.length; i++){
          if(predictions[i].confidence >= CONFIDENCE_THRESHOLD){
            viewController.loadAdvertisement(predictions[i].label, predictions[i].position, predictions[i].size);
          }
        }
      }
    });
  });
}


function hideSourceSelector(){
  document.getElementById('source-selector').style.display = 'none';
}

window.addEventListener('resize', function() {
  refreshContainerSize();
});

function refreshContainerSize() {
  var container_width=document.getElementById('main-container').style.width;
  var container_height = document.getElementById('main-container').style.height;

  document.getElementById('scene').camera.aspect = 1;
  document.getElementById('scene').camera.updateProjectionMatrix();
  document.getElementById('video').style.width = container_width + "px";
  document.getElementById('video').style.height = container_height + "px";
}

// fetch URL GET values
function getValue() {
  var vars = {};
  var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
    vars[key] = value;
  });
  return vars;
}
