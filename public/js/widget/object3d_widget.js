/*
  A Widget to display 3D Objects
*/

class Object3dWidget extends Widget{

  constructor(viewController, data, advertisement, advertisementSize){
    super(viewController, data, advertisement, advertisementSize);
    this.url = data.url;
    this.icon = 'data/images/cube.svg';
  }

  _onLoaded(){}

  _onDestroy(){}

  _createView(data, callback){
    let entity = document.createElement('a-entity');
    entity.setAttribute('obj-model', "obj: url("+data.url+".obj); mtl: url("+data.url+".mtl);");
    entity.setAttribute('scale', '0.2, 0.2, 0.2');

    callback(entity);
  }

}
ScriptLoader.init(Object3dWidget);
