/*
  Specific widget declaration
*/

class ArticleWidget extends Widget{

  constructor(viewController, data, advertisement, advertisementSize){
    super(viewController, data, advertisement, advertisementSize);
  }

  _onLoaded(){}

  _onDestroy(){}

  _createView(data, callback){
    let entity = document.createElement('a-entity');
    entity.setAttribute('htmlembed','');
    entity.appendChild(this._getDOMObject());
    callback(entity);
  }

  _getDOMObject(){
    let article = document.createElement('div');
    article.className = "post";
    let h1 = document.createElement('h1');
    h1.innerHTML = data.title;

    let p = document.createElement('p');
    p.innerHTML = data.text;

    let img = document.createElement('img');
    img.src = data.imgurl;
    img.width = "100";
    article.appendChild(h1);
    article.appendChild(p);
    article.appendChild(img);
    return article;
  }

}
