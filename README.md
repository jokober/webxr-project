# Immersive Web: Markerless Detection and Enrichment of Billboards
The aim of this project was to investigate the ability to enrich the content of billboards using a platform independent solution that runs on mobile as well as on desktop devices without the installation of additional software or plugins.

The project consists of a [client web application](public/) and a [detection server](flask_server/).



## Requirements
*  Python3
*  Chrome 56+ / Firefox 51+ / Operat 66+ / Edge 79+


## Client
To run the client the code of the [public/](public/) directory must be copied on a web server.
Every device that supports JavaScript and fullfills the requirements should be able to run the client. 

## Server
The client only works when the python server is running.
The server detects the advertisements by a supervised-learning algorithm. The training data can be found in the [yolo_training_files](yolo_training_files) directory.
More details how to start the server can be found  [here](flask_server/).