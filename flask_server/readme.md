# Server


The server expects to receive an image by http push request which will then be analyzed using the Yolo image detection model. The server will respond with a json which contains the most important predictions including the labels, confidence and the bounding box coordinates.

## Install Dependencies
* cd flask_server
* pip install -r requirements.txt

## Start Server
* cd flask_server
* python3 detect_bb_flask.py

<hr><br><br>

# Additional Informations

### Http Request
Javascript http request export from postman:
```
var data = new FormData();
data.append("image", "/Users/Ko/PycharmProjects/detect_yolov3_flask/test1.jpg");

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === 4) {
    console.log(this.responseText);
  }
});

xhr.open("POST", "http://127.0.0.1:5000/api/get_predictions");
xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");


xhr.send(data);
```
The size of the image should not be smaller than 416x416 pixels because the model is trained for that size.

### HTTP Response
The http response is structured as a json containing the most important predictions including label, confidence and the relative positions of the bounding boxes. You have to scale the bounding box coordinates back relative to the size of the image by multiplying the the x,y,w,h values with the actual image size. Keep in mind that YOLO actually
returns the center (x, y)-coordinates of the bounding box followed by the boxes' width and height

Example response:
```
[{
  "label": "amnesty",
  "confidence": 0.9999868869781494,
  "position": {
	"x": 0.086,
	"y": 0.391},
  "size": {
	"width": 0.2285,
	"height": 0.27125}
},
{
  "label": "aussenwerbung",
  "confidence": 0.999350368976593,
  "position": {
	"x": 0.5676666666666667,
	"y": 0.46},
  "size": {
	"width": 0.2705,
	"height": 0.29375}
}]
```
